/**
 * @file resizable-sidebar.js
 */

(function ($, Drupal, window, document) {

  'use strict';

  const storageWidth = "Drupal.gin_resizable_sidebar.width";
  const storageOriginalWidth = "Drupal.gin_resizable_sidebar.original_width";

  /**
   * A custom state object for the resize behaviour.
   */
  Drupal.GinResizableSidebar = {
    width: null,
    originalX: null,
    originalBorder: null,
    originalOpacity: null,
    direction: document.dir,
    min: 250,
    dragHandler: null,
    dragging: false,
  };

  /**
   * Initialize the resizable functionality.
   */
  Drupal.GinResizableSidebar.init = function() {
    once('ginResizableSidebar', 'body').forEach(() => {

      if (localStorage.getItem(storageOriginalWidth) === null && this.getGinVariable('sidebar-width')) {
        localStorage.setItem(storageOriginalWidth, parseInt(this.getGinVariable('sidebar-width')));
      }

      let sidebar_width = this.getInitialSidebarWidth();
      this.width = sidebar_width;
      if (sidebar_width != $('#gin_sidebar').width()) {
        this.setSidebarWidth(sidebar_width);
      }
      let border_key = this.direction == 'ltr' ? 'border-left' : 'border-right';
      this.originalBorder = $('#gin_sidebar').css(border_key);
      this.originalOpacity = $('#gin_sidebar > div').css('opacity');
    });
  }

  /**
   * Get the initial sidebar width.
   */
  Drupal.GinResizableSidebar.getInitialSidebarWidth = function() {
    let width = localStorage.getItem(storageWidth) || this.getGinVariable('sidebar-width');
    let max = window.screen.width * 0.8;
    return width > this.min ? (width < max ? width : max) : this.min;
  }

  /**
   * Set the new sidebar width.
   */
  Drupal.GinResizableSidebar.setSidebarWidth = function(width) {

    let max = window.screen.width * 0.8;
    if (width < this.min) {
      width = this.min;
    }
    else if (width > max) {
      width = max;
    }
    this.setGinVariable('sidebar-width', width + 'px');
    this.setGinVariable('sidebar-offset', width + 'px');
    let property_key = this.direction == 'ltr' ? 'right' : 'left';
    $('#gin-sidebar-draggable').css(property_key, 'calc(' + width + 'px - 0.25rem)');
    return width;
  }

  /**
   * Set a gin variable.
   */
  Drupal.GinResizableSidebar.setGinVariable = function(name, value) {
    document.querySelector(':root').style.setProperty('--gin-' + name, value);
  }

  /**
   * Get a gin variable.
   */
  Drupal.GinResizableSidebar.getGinVariable = function(name) {
    return getComputedStyle(document.querySelector(':root')).getPropertyValue('--gin-' + name);
  }

  /**
   * Create the drag handler element.
   */
  Drupal.GinResizableSidebar.createDragHandler = function(name) {
    this.dragHandler = document.createElement('span');
    const id = document.createAttribute('id');
    id.value = 'gin-sidebar-draggable';
    this.dragHandler.setAttributeNode(id);
    return this.dragHandler;
  }

  /**
   * Set the drag border.
   */
  Drupal.GinResizableSidebar.setDragBorder = function(status) {
    let border = status ? '1px solid grey' : this.originalBorder;
    let border_key = this.direction == 'ltr' ? 'border-left' : 'border-right';
    $('#gin_sidebar').css(border_key, border);
  }

  /**
   * Callback for the dragging of the sidebar.
   */
  Drupal.GinResizableSidebar.dragSidebar = function(event) {
    event.preventDefault();
    window.requestAnimationFrame(() => {
      let client_x = event.type == 'touchmove' ? event.touches[0].clientX : event.clientX;
      let resize_offset = parseFloat(client_x - Drupal.GinResizableSidebar.originalX);
      let original_width = parseFloat(Drupal.GinResizableSidebar.width);
      let sidebar_width = original_width - resize_offset;

      if (Drupal.GinResizableSidebar.direction == 'rtl') {
        sidebar_width = original_width + resize_offset;
      }
      Drupal.GinResizableSidebar.setSidebarWidth(sidebar_width);
    });
  }

  /**
   * Callback for starting the resize.
   */
  Drupal.GinResizableSidebar.startResize = function(event) {
    $('#gin_sidebar > div').css('opacity', 0.5);
    this.setDragBorder(true);
    this.dragging = true;
    if (event.type == 'touchstart') {
      this.originalX = event.touches[0].clientX;
      document.addEventListener('touchmove', this.dragSidebar, { passive: false });
    }
    else {
      this.originalX = event.clientX;
      document.addEventListener('mousemove', this.dragSidebar);
    }
  }

  /**
   * Callback for ending the resize.
   */
  Drupal.GinResizableSidebar.endResize = function(event) {
    this.width = $('#gin_sidebar').width();
    $('#gin_sidebar > div').css('opacity', this.originalOpacity);
    this.setDragBorder(false);
    this.dragging = false;
    if (event.type == 'touchend') {
      document.removeEventListener('touchmove', this.dragSidebar, { passive: false });
    }
    else {
      document.removeEventListener('mousemove', this.dragSidebar);
    }
    localStorage.setItem(storageWidth, this.width);
  }

  Drupal.GinResizableSidebar.events = {
    'mousedown': (event) => {
      if (event.button !== 0) {
        return;
      }
      event.preventDefault();
      event.stopPropagation();
      event.stopImmediatePropagation();
      Drupal.GinResizableSidebar.startResize(event);
    },
    'mouseup': (event) => {
      if (!Drupal.GinResizableSidebar.dragging) {
        return;
      }
      event.preventDefault();
      event.stopPropagation();
      event.stopImmediatePropagation();
      Drupal.GinResizableSidebar.endResize(event);
    },
    'dblclick': (event) => {
      let original_width = localStorage.getItem(storageOriginalWidth);
      if (original_width) {
        Drupal.GinResizableSidebar.setSidebarWidth(original_width);
        localStorage.setItem(storageWidth, original_width);
      }
    },
    'touchstart': (event) => {
      if (event.touches.length > 1) {
        return;
      }
      event.stopPropagation();
      event.stopImmediatePropagation();
      Drupal.GinResizableSidebar.startResize(event);
    },
    'touchend': (event) => {
      if (event.touches.length > 1) {
        return;
      }
      event.stopPropagation();
      event.stopImmediatePropagation();
      Drupal.GinResizableSidebar.endResize(event);
    },
  }

  /**
   * Register resizable sidebar behaviours.
   */
  Drupal.behaviors.gin_resizable_sidebar = {
    attach: function (context, settings) {

      once('ginResizableSidebar', '#gin_sidebar', context).forEach(element => {
        Drupal.GinResizableSidebar.init();

        let drag_handler = Drupal.GinResizableSidebar.createDragHandler();
        $(element).before(drag_handler);

        // Add event listeners for desktop.
        drag_handler.addEventListener('mousedown', Drupal.GinResizableSidebar.events.mousedown);
        document.addEventListener('mouseup', Drupal.GinResizableSidebar.events.mouseup);

        // Add event listeners for touchscreen devices.
        drag_handler.addEventListener('touchstart', Drupal.GinResizableSidebar.events.touchstart);
        document.addEventListener('touchend', Drupal.GinResizableSidebar.events.touchend);

        // Allow to reset to the original width set by Gin.
        drag_handler.addEventListener('dblclick', Drupal.GinResizableSidebar.events.dblclick);

        window.addEventListener('resize', () => {
          Drupal.GinResizableSidebar.setSidebarWidth(Drupal.GinResizableSidebar.width);
        });
      });
    }
  };

}(jQuery, Drupal, window, document));
