# Gin Resizable Sidebar

## Purpose

Allow users to resize the Gin sidebar on content edit forms.


## Installation

Just enable the module and that's it. No configuration required.


## Usage

Once enabled, a drag indicator will be shown when hovering with the mouse over the border between the sidebar and the rest of the content. Clicking and dragging will then resize the sidebar. The current size will be stored in the local storage and will be used to reset the sidebar to the chosen width whenever the sidebar opens.
Double clicking on the drag border will reset the sidebar to the original size set by Gin.


## Additional Requirements

This module obviously requires the Gin Admin Theme. If Gin is not enabled, this module will do nothing.